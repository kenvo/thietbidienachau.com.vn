<?php get_header(); ?>
<?php get_sidebar(); ?>
<div id="main-content-page" class="main-content-list">
    <div class="post">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				
				<h3><?php the_title(); ?></h3>
				
				<div class="entry">
					
					<?php the_content(); ?>

				</div>
				
				
			</div>

		<?php endwhile; endif; ?>
	</div><!-- end post -->
</div><!-- end post -->
	

<?php get_footer(); ?>

