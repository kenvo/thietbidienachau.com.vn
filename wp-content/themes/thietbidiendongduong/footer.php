 <div style="clear:left"></div>
        <div class="footer">
            <?php if(pll_current_language() == vi ) { ?>
        	<div class="footer1">
                 <?php if ( is_active_sidebar( 'footer_1' ) ) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar( 'footer_1' ); ?>
                </div><!-- #primary-sidebar -->
                <?php endif; ?>
        	</div>
        	<div class="footer2">
                 <?php if ( is_active_sidebar( 'footer_2' ) ) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar( 'footer_2' ); ?>
                </div><!-- #primary-sidebar -->
                <?php endif; ?>
        	</div>

            <?php } else { ?>
            <div class="footer1">
                 <?php if ( is_active_sidebar( 'footer_1_english' ) ) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar( 'footer_1_english' ); ?>
                </div><!-- #primary-sidebar -->
                <?php endif; ?>
            </div>
            <div class="footer2">
                 <?php if ( is_active_sidebar( 'footer_2_english' ) ) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar( 'footer_2_english' ); ?>
                </div><!-- #primary-sidebar -->
                <?php endif; ?>
            </div>
            <?php } ?>

        	<div class="footer3">
                 <?php if ( is_active_sidebar( 'footer_3' ) ) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar( 'footer_3' ); ?>
                </div><!-- #primary-sidebar -->
                <?php endif; ?>
        	</div>
        	<div class="footer4">
                 <?php if ( is_active_sidebar( 'footer_4' ) ) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar( 'footer_4' ); ?>
                </div><!-- #primary-sidebar -->
                <?php endif; ?>
        	</div>
        </div>
        <?php wp_footer(); ?>
        <?php if ( is_active_sidebar( 'subiz_chat_online' ) ) : ?>
            <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                <?php dynamic_sidebar( 'subiz_chat_online' ); ?>
            </div><!-- #primary-sidebar -->
        <?php endif; ?>
    </div>
</body>
</html>