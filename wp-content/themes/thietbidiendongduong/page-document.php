<?php  
/*
	Template Name: Document
*/
?>
<?php get_header(); ?>
<?php get_sidebar(); ?>
	 <div class="content" id="page-giaiphap" style="background:none;">
	  <h3><?php the_title(); ?></h3>
        	<div class="main-content">
        	    <div class="main-content-list" style="width: 100%;">
                   <?php
                        // TO SHOW THE PAGE CONTENTS
                        while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                            <div class="entry-content-page">
                                <?php the_content(); ?> <!-- Page Content -->
                            </div><!-- .entry-content-page -->

                        <?php
                        endwhile; //resetting the page loop
                        wp_reset_query(); //resetting the page query
                    ?>
        	    </div>
        	</div>
        </div> <!-- end content of content-web -->



<?php get_footer(); ?>
