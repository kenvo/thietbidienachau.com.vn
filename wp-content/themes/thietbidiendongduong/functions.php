<?php
	
	// Add RSS links to <head> section
	automatic_feed_links();
	
	// Load jQuery
	if ( !is_admin() ) {
	   wp_deregister_script('jquery');
	   wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"), false);
	   wp_enqueue_script('jquery');
	}
	
	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
    
	// Declare sidebar widget zone

// create menu suport for theme
if(function_exists('register_nav_menus')) {
    register_nav_menus(
        array(
            'main_nav' => 'Main Navigation Menu'
        )
    );
}

    // create post thumbnail
    if(function_exists('add_theme_support')) {
        add_theme_support('post-thumbnails');
    }


 if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url(); 





/**
 * Register our sidebars and widgetized areas.
 *
 */
function footer1_widgets_init() {

    register_sidebar( array(
        'name'          => 'Footer 1',
        'id'            => 'footer_1',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'footer1_widgets_init' );

function footer1_english_widgets_init() {

    register_sidebar( array(
        'name'          => 'Footer 1 English',
        'id'            => 'footer_1_english',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'footer1_english_widgets_init' );

function subiz_chat_online() {

    register_sidebar( array(
        'name'          => 'subiz_chat_online',
        'id'            => 'subiz_chat_online',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'subiz_chat_online' );


function footer2_widgets_init() {

    register_sidebar( array(
        'name'          => 'Footer 2',
        'id'            => 'footer_2',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'footer2_widgets_init' );

function footer_mobile_vi() {

    register_sidebar( array(
        'name'          => 'footer_mobile_vi',
        'id'            => 'footer_mobile_vi',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'footer_mobile_vi' );

function footer_mobile_en() {

    register_sidebar( array(
        'name'          => 'footer_mobile_en',
        'id'            => 'footer_mobile_en',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'footer_mobile_en' );

function footer2_english_widgets_init() {

    register_sidebar( array(
        'name'          => 'Footer 2 English',
        'id'            => 'footer_2_english',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'footer2_english_widgets_init' );



function footer3_widgets_init() {

    register_sidebar( array(
        'name'          => 'Footer 3',
        'id'            => 'footer_3',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'footer3_widgets_init' );


function footer4_widgets_init() {

    register_sidebar( array(
        'name'          => 'Footer 4',
        'id'            => 'footer_4',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'footer4_widgets_init' );

function contact_widgets_init() {

    register_sidebar( array(
        'name'          => 'Contact',
        'id'            => 'contact',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'contact_widgets_init' );


function contact_english_widgets_init() {

    register_sidebar( array(
        'name'          => 'Contact english',
        'id'            => 'contact_english',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'contact_english_widgets_init' );


function language_home_widget_widgets_init() {

    register_sidebar( array(
        'name'          => 'Language Home Widget',
        'id'            => 'language_home_widget',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'language_home_widget_widgets_init' );

//sidebar
function sidebarBanner() {

    register_sidebar( array(
        'name'          => 'Banner Sidebar Widget',
        'id'            => 'banner_sidebar_widger',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'sidebarBanner' );




/// categories images
$args = array(
  'type'                     => 'post',
  'child_of'                 => 0,
  'parent'                   => $cat,
  'orderby'                  => 'name',
  'order'                    => 'ASC',
  'hide_empty'               => 1,
  'hierarchical'             => 1,
  'exclude'                  => '',
  'include'                  => '',
  'number'                   => '6',
  'taxonomy'                 => 'category',
  'pad_counts'               => false 

);

$category_child = get_categories($args);

$argss = array(
  'type'                     => 'post',
  'child_of'                 => 0,
  'parent'                   => $catid,
  'orderby'                  => 'name',
  'order'                    => 'ASC',
  'hide_empty'               => 1,
  'hierarchical'             => 1,
  'exclude'                  => '',
  'include'                  => '',
  'number'                   => '6',
  'taxonomy'                 => 'category',
  'pad_counts'               => false 

);


// $category = get_the_category(); 
// $parent = get_ancestors($category[0]->term_id,'category');
// if (empty($parent)) {
//   $parent[] = array($category[0]->term_id);
// }
// $parent = array_pop($parent);
// $parent = get_category($parent); 
// if (!is_wp_error($parent)) {
//   var_dump($parent);
// } else {
//   echo $parent->get_error_message();
// }
// var_dump($category_parent);





// foreach ($category_child->$category_child_cat) {
//     var_dump($category_child_cat);
// }

$cha = get_categories($argss);

$categories_parent = array();
foreach ($category_child as $category_child_value) {
// echo "<pre>";
// var_dump($category_child_value);
// echo "</pre>";
$categories_parent[] = get_category_parents($category_child_value->cat_ID,'');
// echo "<pre>";
// var_dump($category_child_value);
// echo "</pre>";
}
// echo "<pre>";
// var_dump($categories_parent);
// echo "</pre>";

// end categories images


function language_home_widget_mobile_widgets_init() {

    register_sidebar( array(
        'name'          => 'Language Home Widget Mobile',
        'id'            => 'language_home_widget_mobile',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'language_home_widget_mobile_widgets_init' );





function register_news_taxonomies()
    {
        register_taxonomy('category_news', 'news', array(
            'label'        => 'News Categories',
            'hierarchical' => true,
        ));
    }
add_action('init','register_news_taxonomies');

function wpdocs_codex_custom_init() {
    $args = array(
        'public' => true,
        'labels'  => array(
            'name'          => __('News'),
            'singular_name' => __('news'),
            'all_items'     => 'List news',
            'add_new'       => 'Create news',
        ),
        'supports'    => array('title', 'editor', 'thumbnail', 'excerpt')
    );
    register_post_type( 'news', $args );
}
add_action( 'init', 'wpdocs_codex_custom_init' );


function register_customer_mobile_taxonomies()
    {
        register_taxonomy('category_mobile', 'mobile', array(
            'label'        => 'Mobile Categories',
            'hierarchical' => true,
        ));
    }
add_action('init','register_customer_mobile_taxonomies');

function register_customer_mobile_init() {
    $args = array(
        'public' => true,
        'labels'  => array(
            'name'          => __('Mobile'),
            'singular_name' => __('mobile'),
            'all_items'     => 'List mobile',
            'add_new'       => 'Create mobile',
        ),
        'supports'    => array('title', 'editor', 'thumbnail', 'excerpt')
    );
    register_post_type( 'mobile', $args );
}
add_action( 'init', 'register_customer_mobile_init' );


function register_slider_home() {
    $args = array(
        'public' => true,
        'labels'  => array(
            'name'          => __('Slider'),
            'singular_name' => __('slider'),
            'all_items'     => 'List slider',
            'add_new'       => 'Create slider',
        ),
        'supports'    => array('title', 'editor', 'thumbnail', 'excerpt')
    );
    register_post_type( 'slider', $args );
}
add_action( 'init', 'register_slider_home' );

function register_slider_taxonomies()
    {
        register_taxonomy('category_slider', 'slider', array(
            'label'        => 'Slider Categories',
            'hierarchical' => true,
        ));
    }
add_action('init','register_slider_taxonomies');

function register_images_home() {
    $args = array(
        'public' => true,
        'labels'  => array(
            'name'          => __('Images'),
            'singular_name' => __('images'),
            'all_items'     => 'List images',
            'add_new'       => 'Create images',
        ),
        'supports'    => array('title', 'editor', 'thumbnail', 'excerpt')
    );
    register_post_type( 'images', $args );
}
add_action( 'init', 'register_images_home' );

function register_images_taxonomies()
    {
        register_taxonomy('category_images', 'images', array(
            'label'        => 'Slider Categories',
            'hierarchical' => true,
        ));
    }
add_action('init','register_images_taxonomies');

?>

