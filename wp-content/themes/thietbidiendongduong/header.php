<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
	
	<link rel="shortcut icon" href="/favicon.ico">
	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>

	<?php wp_head(); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_directory'); ?>/scss/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_directory'); ?>/scss/down.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
</head>

<body <?php body_class(); ?>>

	<div id="page-wrap">

		<div id="header">
		    <div class="logo_home">
			    <a href="<?php echo get_option('home') ?>">
			       <?php
					    global $post;
					    $args = array('numberposts'=>1,'post_type'=>'images');
					    $custom_posts = get_posts($args);
					    foreach($custom_posts as $post) : setup_postdata($post);
					        the_post_thumbnail();
					    endforeach;
					    wp_reset_postdata();
					    
					?>
			    </a>
			    <span class="name_site_bloginfo">
			    <?php 
			    	if(pll_current_language() == vi ){
			    		bloginfo('name');

			    	}else{ ?>
						<span>
							ASIA TRADING AND ENGINEERING COMPANY LIMITED
						</span>
			    <?php	}?>
			    </span>
			</div>
			<div class="language_home">
                <?php if ( is_active_sidebar( 'language_home_widget' ) ) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar( 'language_home_widget' ); ?>
                </div><!-- #primary-sidebar -->
                <?php endif; ?>
			</div>
		</div>
		<div class="menu">
        	<?php echo (pll_current_language() == vi) ? wp_nav_menu( array('menu'=>'Page Menu', 'container'=>'')) : 
        	wp_nav_menu( array('menu'=>'Page Menu English', 'container'=>'')); ?>
        </div>