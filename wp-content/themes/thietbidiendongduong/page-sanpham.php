<?php /* Template Name: sanpham */ ?>
<?php get_header(); ?>
<?php get_sidebar(); ?>
	 <div class="content-view-content">
                  <h3><?php echo (pll_current_language() == vi ) ? 'Trang chủ' : 'Your present location Home' ?></h3>
                  <ul>




                  <?php
                      global $post;
                      $args = array('post_type'=>'post', 'posts_per_page'=>24);
                      $custom_posts = get_posts($args);
                      foreach($custom_posts as $post) : setup_postdata($post);
                  ?>
                       <li>
                           <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                           <h4 style="margin:0;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                      </li>
                   <?php
                      endforeach;
                      wp_reset_postdata();

                  ?>



                  </ul>
              </div>
          </div>
    </div>

<?php get_footer(); ?>
