<div class="content-view">
    <div class="list-menu-content-view">
        <div class="content-view-menu">
            <h3><?php echo (pll_current_language() == vi ) ? 'DANH MỤC SẢN PHẨM' : 'PRODUCTS LIST'; ?></h3>

            <?php echo (pll_current_language() == vi ) ? wp_nav_menu(array('menu'=>'Category Menu View', 'container'=>'')) : 
              wp_nav_menu(array('menu'=>'Category Menu View English', 'container'=>'')) ?>

            <div class="search_view">
               <?php get_search_form(); ?>
            </div>

            <div class="banner_sidebar">
            	<h3><?php echo (pll_current_language() == vi ) ? 'ĐỐI TÁC CÔNG TY' : 'PARTNER OF THE COMPANY'; ?></h3>
            	<?php
            		if (is_active_sidebar('banner_sidebar_widger')) {
						dynamic_sidebar('banner_sidebar_widger');
					}
            	?>
            </div>
        </div>