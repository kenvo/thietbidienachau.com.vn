 <div class="slider">
        	 <div id="myCarousel" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			    <li data-target="#myCarousel" data-slide-to="1"></li>
			    <li data-target="#myCarousel" data-slide-to="2"></li>
			    <li data-target="#myCarousel" data-slide-to="3"></li>
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">


			  <?php 
			    global $post;
			    $argsss = array('numberposts'=>30, 'post_type'=>'slider', 'order'=>'DESC');
			    $custom_posts = get_posts($argsss);
			    foreach($custom_posts as $post) : setup_postdata($post);

			   ?>
			    <div class="item">
			      <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
			      <div class="carousel-caption">
			      </div>
			    </div>
			   <?php
			    endforeach;
			    wp_reset_postdata();
			    ?>
    
			   
			  </div>

			  <!-- Left and right controls -->
			  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
        </div>  <!-- end content of slider -->
        <script>
        	$(document).ready(function(){
        		$('div.item:first').addClass('active');
        	});
        </script>