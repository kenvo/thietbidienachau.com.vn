<?php get_header(); ?>
<?php include(TEMPLATEPATH . '/inc/featured.php') ?>
   <div class="content">
          <div class="Category_product_home_title">
             <div class="Category_product_home_title_left" ><?php echo (pll_current_language() == vi ) ? 'DANH MỤC SẢN PHẨM' : 'PRODUCTS LIST'; ?></div>
             <div class="Category_product_home_title_right" ><?php echo (pll_current_language() == vi ) ? 'TIN TỨC' : 'NEWS'; ?></div>
             <div class="Category_product_home_link_page_news" ><a <?php if(pll_current_language() == vi) {  ?> href="<?php get_home_url(); ?>/?page_id=164" <?php } else { ?> href="<?php get_home_url(); ?>/?page_id=317" <?php } ?>><?php echo (pll_current_language() == vi ) ? 'Xem Thêm' : 'MORE'; ?></a></div>
          </div>
          <div class="main-content">
              <div class="main-content-gift">
                   <div>
                       <ul>
                      
                          <?php
                          $parentscategory ="";
                          $cat_get = get_categories($args);
                          foreach($cat_get as $category) {
                              if ($category->category_parent == 0) {
                              $parentscategory .= ' <a href="' . get_category_link($category->cat_ID) . '" title="' . $category->name . '">' . $category->name . '</a>, ';
                                // var_dump(get_categories($args));
                              }
                          }

                         foreach ($cat_get as &$category) {
                            $category->image = z_taxonomy_image_url($category->term_id);
                            $category->link  = get_category_link($category->term_id);
                         }


                          ?>
                         
                              <?php
                                  foreach ($cat_get as $cat_get_value) {
                              ?>
                               <li>
                                    <a href="<?php echo $cat_get_value->link; ?>"> 
                                        <img src="<?php echo $cat_get_value->image; ?>" />   
                                        <p class="value_cat_home_parent"><?php echo $cat_get_value->name; ?></p>
                                     </a>
                               </li>
                              <?php  } ?> 
                       </ul>
                   </div>
              </div>
              <div class="main-content-list">
                      <ul>
                      <?php (pll_current_language() == vi) ? query_posts('posts_per_page=5&post_type=news') : query_posts('posts_per_page=5&type_post=news');  ?>
                      <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                          <li>
                          <span class="glyphicon glyphicon-stop">
                          </span>
                            <a href="<?php the_permalink(); ?>">
                           <?php
                              $string1=get_the_title();
                              if(strlen($string1)>30){
                                      $vitrib=29;
                                      $substr=substr($string1,30);
                                      $vitric=strpos(substr($string1,30)," ");
                                      if($vitric!=false){
                                        $vitrib=30+$vitric;
                                        $string1=substr($string1,0,$vitrib);
                                        echo $string1;
                                      }else{
                                        echo $string1;
                                      }
                              }else{
                                the_title();
                              }
                             ?>
                           ...</a><span class="date-time-now"><?php the_time('d-m-Y') ?></span></li>
                      <?php endwhile; else : ?>
                      <?php endif; wp_reset_query(); ?>
                      </ul>
              </div>
              <div class="main-content-images">
                  <img src="<?php echo bloginfo('template_directory'); ?>/images/sidebar-right.jpg" alt="">
              </div>
          </div>
        </div> <!-- end content of content-web -->

<?php get_footer(); ?>
