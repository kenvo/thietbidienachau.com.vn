<?php get_header(); ?>
<?php get_sidebar(); ?>
    <div class="content-view-content">
		<?php if (have_posts()) : ?>

 			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

			<?php /* If this is a category archive */ if (is_category()) { ?>
				<h2>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h2>

			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<h2>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2>

			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<h2>Archive for <?php the_time('F jS, Y'); ?></h2>

			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<h2>Archive for <?php the_time('F, Y'); ?></h2>

			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<h2>Archive for <?php the_time('Y'); ?></h2>

			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<h2>Author Archive</h2>

			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<h2>Blog Archives</h2>
			
			<?php } ?>

			
	        <h3><?php the_title(); ?></h3>
	        <?php while(have_posts()) : the_post(); ?>
	        	<li>
	               <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
	               <h4><a href=""><?php the_title(); ?></a></h4>
	            </li>
	            <div style="clear:left"></div>
	        <?php endwhile; else : ?>
	        <h3><?php echo (pll_current_language() == vi) ? 'Không có sản phẩm' : 'Nothing found' ?></h3>
	        <?php endif; ?>

    </div>

 <?php  while(have_posts()) : the_post(); ?> 
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                <?php the_excerpt(); ?>
                <p class="meta-data">By: <strong><?php the_author(); ?></strong> | On: <?php the_time('F j, Y'); ?></p>               
        <?php endwhile; else : ?>
			<h3><?php echo (pll_current_language() == vi) ? 'Không có sản phẩm' : 'Nothing found' ?></h3>
	<?php endif; ?>

<?php get_footer(); ?>




