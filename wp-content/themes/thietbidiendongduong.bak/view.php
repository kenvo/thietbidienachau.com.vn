<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>new.upolymer.com/</title>
	<link rel="stylesheet" type="text/css" href="scss/style.css">
	<link rel="stylesheet" type="text/css" href="scss/bootstrap.min.css">
</head>
<body>
    <div id="wrapper">
        <div class="header">
            <a href="index.php"><img src="images/logo.jpg" alt=""></a>
        </div>
        <div class="menu">
        	<ul>
        		<li><a href="">Trang chủ</a></li>
        		<li><a href="">Giới thiệu</a></li>
        		<li><a href="">Sản phẩm</a></li>
        		<li><a href="">Tài liệu</a></li>
        		<li><a href="">Giải pháp</a></li>
        		<li><a href="">Liên hệ</a></li>
        	</ul>
        </div>

        <div class="content-view">
            <div class="list-menu-content-view">
                <div class="content-view-menu">
                    <h3>DANH MỤC SẢN PHẨM</h3>
                    <ul>
                        <li><a href=""> Vít liền đệm phẳng</a></li>
                        <li><a href=""> Tụ bù và cuận kháng</a></li>
                        <li><a href=""> Thiết bị điện hạ thế</a></li>
                        <li><a href=""> Thanh cái mềm </a></li>
                        <li><a href=""> Tán rút</a></li>
                        <li><a href=""> Switch Position Indicator</a></li>
                        <li><a href=""> Ống co ngót cách điện</a></li>
                        <li><a href=""> Hộp đầu cáp, hộp nối</a></li>
                        <li><a href=""> Đồng thanh cái bọc</a></li>
                        <li><a href=""> Đồng hồ đo chức năng</a></li>
                        <li><a href=""> Cầu đấu đế cầu chì</a></li>
                        <li><a href=""> Cầu chì cao thế</a></li>
                        <li><a href=""> Biến dòng điện</a></li>
                        <li><a href=""> Công tơ nhiều biểu</a></li>
                    </ul>
                    <div class="search_view">
                        <img src="images/view/search.jpg" alt="">
                        <p>Từ khóa:</p>
                        <input type="text" name="" value="">
                        <input type="submit" name="" value="">
                        <img src="images/view/linhe.jpg" alt="">
                    </div>
                </div>
                <div class="content-view-content">
                    <h3>SẢN PHẨM</h3>
                    <ul>
                        <li>
                           <a href=""><img src="images/view/1.jpg" alt=""></a>
                           <h4><a href=""></a></h4>
                        </li>
                         <li>
                           <a href=""><img src="images/view/2.jpg" alt=""></a>
                           <h4><a href=""></a></h4>
                        </li>
                         <li>
                           <a href=""><img src="images/view/3.jpg" alt=""></a>
                           <h4><a href=""></a></h4>
                        </li>
                         <li>
                           <a href=""><img src="images/view/4.jpg" alt=""></a>
                           <h4><a href=""></a></h4>
                        </li>
                         <li>
                           <a href=""><img src="images/view/5.jpg" alt=""></a>
                           <h4><a href=""></a></h4>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
        <div style="clear:left"></div>
        <div class="footer">
        	<div class="footer1">
        	    <strong>Công ty cổ phần Đa phương tiện VMMS</strong>
        	    <p>Địa chỉ: Phòng 107, tòa nhà media Láng Hạ - Hà Nội - Việt Nam</p>
        	</div>
        	<div class="footer2">
        		<p>Điện thoại: 01672178429</p>
        		<p>E-mail: quangsen283@gmail.com</p>
        	</div>
        	<div class="footer3">
        		<p>-Fax: +01672178429</p>
        	</div>
        	<div class="footer4">
        		<p>Copyright ATEN LTD</p>
        		<p>2007 - 2015</p>
        	</div>
        </div>
    </div>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>