<?php get_header(); ?>
<?php get_sidebar(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.elevatezoom.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/img.zoom.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jssor.slider.mini.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/slider_with.js"></script>
<div  id="main-content-page" class="main-content-list" >




    <?php if(in_category( 179, get_the_ID() ) || in_category( 177, get_the_ID() )){ ?>
    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
        <h3 style="padding-bottom:30px;"><?php echo (pll_current_language() == vi) ? 'Tài Liệu' : 'Document'; ?></h3>
        <p><?php if(pll_current_language() == vi) { echo "<a href='http://thietbidien.vicoders.com/'>Trang chủ</a>  >> "; } else { echo "<a href='#'>Home</a> >> "; } the_title(); ?> </p>
        <h1 id="title-download"><?php the_title(); ?></h1>
        <div id="single_content" class="single_content_down">
            <div class="single_content_left">
                <?php the_post_thumbnail(); ?>
            </div>
            <div class="single_content_right document-width">
                <p><?php the_content(); ?></p>
                <div class="descriptions-down">
                    <p class="title-description">File description</p>
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <p><?php the_content(); ?></p>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>


<?php } else{   
?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			
			<h3><?php echo (pll_current_language() == vi ) ? 'SẢN PHẨM' : 'PRODUCTS'; ?></h3>
            <p><?php if(pll_current_language() == vi) { echo "<a href='http://thietbidien.vicoders.com/'>Trang chủ</a>  >> "; } else { echo "<a href='#'>Home</a> >> "; } the_title(); ?> </p>
            <div id="single_content">
                <div class="single_content_left">
                    <?php
                        $src_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
                        $src_medium = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
                        $src_large = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                    ?>
                    <img style="border: 1px solid rgb(232, 232, 230); position: inherit !important;" id="zoom_03" src="<?php echo $src_medium['0']; ?>" data-zoom-image="<?php echo $src_large['0']; ?>" width="411">
                    <div id="gallery_01">
                     <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 809px; height: 150px; overflow: hidden; visibility: hidden;">
                        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 809px; height: 150px; overflow: hidden;">
                            <div style="display: none;">
                                <a href="#" class="elevatezoom-gallery active" data-image="<?php echo $src_medium['0']; ?>" data-zoom-image="<?php echo $src_large['0']; ?>">
                                    <img data-u="image" src="<?php echo $src_thumbnail['0']; ?>" width="100">
                                </a>
                            </div>
                                <?php $attachments = new Attachments( 'attachments' ); /* pass the instance name */ ?>
                                <?php if( $attachments->exist() ) : ?>
                                <?php while( $attachments->get() ) : ?>
                                <div style="display: none;">
                                    <a href="#" class="elevatezoom-gallery active " data-image="<?php echo $attachments->src( 'medium' ); ?>" data-zoom-image="<?php echo $attachments->src( 'large' ); ?>">
                                        <img data-u="image" src="<?php echo $attachments->src( 'thumbnail' ); ?>" width="100">
                                    </a>
                                </div>
                                <?php endwhile; ?>
                                <?php endif; ?>
                        </div>
                        <span data-u="arrowleft" class="jssora03l" data-autocenter="2"></span>
                        <span data-u="arrowright" class="jssora03r"  data-autocenter="2"></span>
                    </div>
                    </div>
                </div>
                 <div class="single_content_right">
                     <p><?php echo (pll_current_language() == vi ) ? 'Tên sản phẩm: ' : 'Product name: '; ?> <?php the_title(); ?></p>
                     <p><?php echo (pll_current_language() == vi ) ? 'Mô tả: ' : 'Decription: '; ?> <?php echo  get_field('mo_ta'); ?> </p>
                     <p><?php echo (pll_current_language() == vi ) ? 'Nhãn hiệu/Xuất xứ: ' : 'Maker: '; ?> <?php echo get_field('nhan_hieu'); ?></p>
                     <p><?php echo (pll_current_language() == vi ) ? 'Bảo hành: ' : 'Warranty'; ?>  <?php echo get_field('bao_hanh') ?></p>
                     <p><?php echo (pll_current_language() == vi ) ? 'Tình trạng: ' : 'Inventory status: '; ?>  <?php echo get_field('tinh_trang') ?></p>
                     <p><?php echo (pll_current_language() == vi ) ? 'Số lượng: ' : 'Quantity: '; ?>  <?php echo the_field('so_luong'); ?></p>
                </div>
            </div>
           
		</div>
    
	<?php endwhile; endif; ?>
    <div style="clear:left"></div>
    <div id="information_product">
        <h3><?php echo (pll_current_language() == vi ) ? 'Thông tin sản phẩm' : 'specification'; ?></h3><div class="information_product_tt"></div>
    </div>
    <div id="information_product_single">
        <p><?php the_content(); ?></p>
    </div>
    <div id="list_information_product_single">
        <h4><?php echo (pll_current_language() == vi ) ? 'Sản phẩm cùng loại' : 'Similar products'; ?></h4>
        <ul>
            <?php $query = new WP_Query( array( 'posts_per_page' => 4, 'cat'=>'-170,-163', 'orderby'=>'rand' )); ?>
            <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                 <li>
                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                    <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                </li>
             <?php endwhile; else: ?>
             <h4><?php echo (pll_current_language() == vi) ? 'Không có sản phẩm' : 'Not found'; ?></h4>
            <?php wp_reset_postdata(); ?>
             <?php endif; ?>
        </ul>
    </div>
<?php } ?>
</div>


<?php get_footer(); ?>
