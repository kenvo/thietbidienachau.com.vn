<?php /* Template Name: Giai phap */ ?>
<?php get_header(); ?>
<?php get_sidebar(); ?>
	 <div class="content" id="page-giaiphap" style="background:none;">
	  <h3><?php the_title(); ?></h3>
        	<div class="main-content">
        	    <div class="main-content-list" style="width: 100%;">
                   <?php //echo (pll_current_language() == vi) ? wp_nav_menu(array('menu'=>'Menu-Page_GiaiPhap', 'container'=>'')) :
                    //wp_nav_menu(array('menu'=>'Menu-Page_GiaiPhap_english', 'container'=>'')); ?>

                    <ul>
                    <?php //cach thuc an toan nhat de tao mot loop rieng biet hoac nhieu loop tren mot trang
					    global $post;
					    if (pll_current_language() == vi) {
					        $args = array('numberposts'=>15, 'orderby'=>'DESC', 'post_type'=>'news', 'category_news'=>'News_vi');
					    } else {
					    	$args = array('numberposts'=>15, 'orderby'=>'DESC', 'post_type'=>'news', 'category_news'=>'News_en');
					    }
					    $custom_posts = get_posts($args);
					    foreach($custom_posts as $post) : setup_postdata($post);
					    ?>
                           <li><a href="<?php the_permalink(); ?>"><span class="glyphicon glyphicon-stop"></span> <?php the_title(); ?></a></li>
                        <?php
					    endforeach;
					    wp_reset_postdata();
					    
					?>
					</ul>
        	    </div>
        	</div>
        </div> <!-- end content of content-web -->


<?php get_footer(); ?>


