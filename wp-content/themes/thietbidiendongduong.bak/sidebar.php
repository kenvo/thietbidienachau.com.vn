<div class="content-view">
    <div class="list-menu-content-view">
        <div class="content-view-menu">
            <h3><?php echo (pll_current_language() == vi ) ? 'DANH MỤC SẢN PHẨM' : 'PRODUCTS LIST'; ?></h3>

            <?php echo (pll_current_language() == vi ) ? wp_nav_menu(array('menu'=>'Category Menu View', 'container'=>'')) : 
              wp_nav_menu(array('menu'=>'Category Menu View English', 'container'=>'')) ?>

            <div class="search_view">
               <?php get_search_form(); ?>
            </div>
        </div>