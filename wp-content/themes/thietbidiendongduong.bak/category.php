<?php get_header(); ?>
<?php get_sidebar(); ?>

  <div class="content-view-content">
                  <h3><?php echo (pll_current_language() == vi) ? 'Các sản phẩm' : 'PRODUCTS'; ?></h3>
                  <?php echo in_category(); ?>
                  <p><?php echo (pll_current_language() == vi) ? 'Trang chủ' : 'Your present location:Home'; ?> >> 
                  <?php if(is_category('17') || is_category('81') ) 
                        { echo (pll_current_language() == vi) ? 'Sản phẩm' : 'Products';} 
                  else { the_category( ', '); } ?></p>
                  <ul>                                
                  <?php if( have_posts()) : while(have_posts()) : the_post(); ?>                                        
                    <li>
                         <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                         <h4 style="margin:0;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    </li>
                  <?php endwhile; else : ?>
                  <h3><?php echo (pll_current_language() == vi ) ? 'Không có sản phẩm' : 'Nothing found' ?></h3>
                  <?php endif ?>
                  <?php wp_pagenavi(); ?>
                  </ul>
              </div>
          </div>
        </div>
<?php get_footer(); ?>