<?php get_header(); ?>
    <div id="file_category">
        <h4><?php echo get_cat_name($cat); ?></h4>
        <ul>
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
            <li>
            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            </li>
        <?php endwhile; else : ?>
        <h3>Nothing found</h3>
        <?php endif; ?>
        <?php wp_pagenavi(); ?>
        </ul>
   </div>
<?php get_footer(); ?>
<?php get_header(); ?>
<?php get_sidebar(); ?>
