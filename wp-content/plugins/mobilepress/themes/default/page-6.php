<?php get_header(); ?>
<div id="contact_us">
    <h2><?php the_title(); ?></h2>
    <h4>Union Polymer Material Co., Ltd</h3>
    <p>Tel（Office hour）:</p>
    <h3><a href="tel:0915266854"><span class="glyphicon glyphicon-earphone"></span>  0915266854</a></h3>
    <h3><a href="tel:01672178429"><span class="glyphicon glyphicon-earphone"></span>  01672178429</a></h3>
    <h3><a href="tel:09666394484"><span class="glyphicon glyphicon-earphone"></span>  09666394484</a></h3>
    <h3><a href="tel:01283492390"><span class="glyphicon glyphicon-earphone"></span>  01283492390</a></h3>
    <p>Tel (24 hours): </p>
    <h3><a href="tel:09342342343"><span class="glyphicon glyphicon-earphone"></span>  09342342343</a></h3>
    <p>Fax: 86-411-87166668</p>
    <p>E-mail: <a href="">info@upolymer.com</a></p>
    <p>Website: <a href="">http://www.upolymer.com </a></p>
    <p>Wechat: welldonelily</p>
    <p class="skype_contact"><img src="<?php bloginfo('template_directory') ?>/img/skype.png" alt=""><a href="">welldonelily</a></p>
    <p class="canhcut_contact"><img src="<?php bloginfo('template_directory') ?>/img/canhcut.jpg" alt=""><a href="tel:9534593459">9534593459</a></p>
    <p>Add: Tangjia Village, Shihe Street, Puwan New District, Dalian,116101 China</p>
   
</div>
<?php get_footer(); ?>
