<?php get_header(); ?>



    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

		</div>
	<?php endwhile; endif; ?>

<div id="information_single_post">
    <h3><?php the_title(); ?></h3>
    <p><a href=""><?php the_post_thumbnail(); ?></a></p>
    <p><?php the_content(); ?></p>
</div>
<?php get_footer(); ?>