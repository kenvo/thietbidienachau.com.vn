<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>
		<?php
			global $query_string;

			if ( is_home() )
				bloginfo( 'name' );

			if ( get_search_query() )
				echo 'Results for: "' . get_search_query() .'"';

			if ( is_month() )
				the_time('F Y');

			if ( is_category() )
				single_cat_title();

			if ( is_single() )
				the_title();

			if ( is_page() )
				the_title();

			if ( is_tag() )
				single_tag_title();

			if ( is_404() )
				echo 'Page Not Found!';
		?>
	</title>
	<meta name="generator" content="WordPress and MobilePress" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link href="<?php bloginfo( 'template_url' ); ?>/css/style.min.css" rel="stylesheet" type="text/css" media="screen, handheld, print, projection" />
	<link rel="icon" type="image/gif" href="<?php bloginfo( 'template_url' ); ?>/img/favicon.gif" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
	<script type="text/javascript">window.addEventListener('load',function(){setTimeout(function(){window.scrollTo(0, 0);},0);});</script>
	<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_directory'); ?>/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>

</head>
<body>

<div id="headerwrap">
	<div id="header">
	    <div id="logo_moblie_home">
		   <a href="<?php echo get_option('home'); ?>">
		      <?php
				    global $post;
				    $args = array('numberposts'=>1,'post_type'=>'images');
				    $custom_posts = get_posts($args);
				    foreach($custom_posts as $post) : setup_postdata($post);
				        the_post_thumbnail();
				    endforeach;
				    wp_reset_postdata();
				    
				?>
		   </a>
		
		</div>
		<div id="language_mobile_home">
		    <span class="glyphicon glyphicon-search"></span>
		    <span class="glyphicon glyphicon-globe"></span>
            <div class="content_language_home_widget_mobile">
			    <?php if ( is_active_sidebar( 'language_home_widget_mobile' ) ) : ?>
	            <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
	                <?php dynamic_sidebar( 'language_home_widget_mobile' ); ?>
	            </div><!-- #primary-sidebar -->
	            <?php endif; ?>
            </div>
            <div class="search_language_home_widget_mobile" >
                <?php get_search_form(); ?>
            </div>
            <script>
                $(document).ready(function() {
                    $('.glyphicon-globe').click(function() {
                        $('.content_language_home_widget_mobile').stop().slideToggle();
                    });
                    $('.glyphicon-search').click(function() {
                        $('.search_language_home_widget_mobile').stop().slideToggle();
                    });
                });
            </script>
           
		</div>
	</div>
	<div id="contentwrap">
		<div id="title" class="contentwrap-title">
			<?php echo (pll_current_language() == vi) ? wp_nav_menu(array('menu'=>'Page Menu Mobile')) : 
			wp_nav_menu(array('menu'=>'Page Menu Mobile English')); ?>
		</div>
