<?php get_header(); ?>
<?php // get_sidebar(); ?>

<div class="content-view-content" id="content-view-content_search_search">
    <h3><?php echo (pll_current_language() == vi ) ? 'Kết quả Search:' : 'Search results:'; ?> </h3>
    <ul>
    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <li>
        	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
        	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        </li>
    <?php endwhile; else: ?>
        <h3><?php echo (pll_current_language() == vi ) ? 'Không có sản phẩm' : 'Nothing found!' ?></h3>
    <?php endif; ?>
	</ul>
</div>
<?php get_footer(); ?>
