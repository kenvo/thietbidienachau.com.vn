		<div id="footerwrap" class="footer_single_mobile">
	        <div id="footer">
			    <?php //cach thuc an toan nhat de tao mot loop rieng biet hoac nhieu loop tren mot trang
				    global $post;
				    
				        $args = array('numberposts'=>2, 'post_type'=>'mobile', 'order'=>'DESC');

				    $custom_posts = get_posts($args); 
				    if (pll_current_language() == vi ) {
				      echo $custom_posts[1]->post_content;
				    } else {
				      echo $custom_posts[0]->post_content;
				    }

				 
				    wp_reset_postdata();
				?>		
			</div>
		</div>
		<div class="scroll-fiexd">
			<div class="scroll mobile-ft " id="page-<?php echo the_ID(); ?>">
			   <ul>
			   	<li><a href="tel:0973291893"><span class="glyphicon glyphicon-earphone"></span> Gọi</a></li>
			   	<li><a href="<?php get_home_url(); ?>/?page_id=385"><span class="glyphicon glyphicon-envelope"></span> Thư</a></li>
			   	<li id="button-share"><a><span class="glyphicon glyphicon-share"></span> Chia sẻ</a></li>
			   	<li><a href="<?php get_home_url(); ?>/?page_id=386"><span class="glyphicon glyphicon-pencil"></span> Thông điệp</a></li>
			   </ul>
			</div>

			<div id="sidebar-share">

				<?php echo do_shortcode('[shareaholic app="share_buttons" id="22434583"]'); ?>

			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#button-share').click(function(){
					$('.shareaholic-share-buttons-container').slideToggle();
					// console.log('aaaaa');
				});
			});

		</script>
	</body>
</html>