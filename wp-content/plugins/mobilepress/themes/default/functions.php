<?php
/// categories images
$args = array(
  'type'                     => 'post',
  'child_of'                 => 0,
  'parent'                   => '0',
  'orderby'                  => 'name',
  'order'                    => 'ASC',
  'hide_empty'               => 1,
  'hierarchical'             => 1,
  'exclude'                  => '17, 170,163,81,177,179',
  'include'                  => '',
  'number'                   => '6',
  'taxonomy'                 => 'category',
  'pad_counts'               => false 

);

$categories = get_categories($args); 
// end categories images

function language_home_widget_mobile_widgets_init() {

    register_sidebar( array(
        'name'          => 'Language Home Widget Mobile',
        'id'            => 'language_home_widget_mobile',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'language_home_widget_mobile_widgets_init' );



 // Declare sidebar widget zone
    if (function_exists('register_sidebar')) {
      register_sidebar(array(
        'name' => 'Right Sidebar',
        'id'   => 'right-sidebar',
        'description'   => 'These are widgets for the sidebar.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
      ));
    }




?>